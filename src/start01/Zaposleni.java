package start01;

public class Zaposleni {
    // polja ili atributi opisuju stanje objekta jedne klase
    // enkapsulacija polja: zastita nad poljima, polja su privatna 
        //i samim tim su polja nedostupna van klase
    // vrednost polji tipa String je null
    private String ime;
    private String prezime;
    private String radnoMesto;
    private String brojRadneKnjizice;
    
    // inicijalizovana vrednost na pocetku je 0.0
    private double plata;
    
    // Konstruktor - metod koji se poziva kada se kreira objekat
    // Ako konstruktor postoji svaki sledeci kreiramo po potrebi, 
        //ako ga ne napravimo java definise automatski konstruktor
    //this - referenca na polje objekta
    public Zaposleni(String ime, String prezime, String radnoMesto, double plata, String brojRadneKnjizice){
        this.ime = ime;
        this.prezime = prezime;
        this.radnoMesto = radnoMesto;
        this.plata = plata;
        this.brojRadneKnjizice = brojRadneKnjizice;
      }
    public Zaposleni(){
        
    }
    /* "Get metodi" - metodi koji vracaju korisniku ove klase deo trenutnog 
    stanja objekta. Primena ovih metoda je kada se izvrsi enkapsulacija polja(kada su polja privatna) 
    */
    public String getIme(){
        return ime;
    }
    public String getPrezime(){
        return prezime;
    }
    public String getRadnoMesto(){
        return radnoMesto;
       }
    public double getPlata(){
        return plata;
    }
    public String getBrojRadneKnjizice(){
        return brojRadneKnjizice;
    }
    
    public int getDatumZaposlenja(){
        String datumZaposlenja = "20" + brojRadneKnjizice.@;
    }
    /* "Set metodi" - metodi koji na zahtev korisnika ove klase menjaju deo trenutnog
    stanja objekta. Primena ovih metoda je kada se izvrsi enkapsulacija polja(kada su polja privatna)
    */
    public void setIme(String ime){
        this.ime = ime;
    }
    public void setPrezime(String prezime){
        this.prezime = prezime;
    }
    public void setRadnoMesto(String radnoMesto){
        this.radnoMesto = radnoMesto;
    }
    public void setPlata(double plata){
        this.plata = plata;
    }
    public void setBrojRadneKnjizice(String brojRadneKnjizice){
        this.brojRadneKnjizice = brojRadneKnjizice;
    }
    /* metod toString()
    generise String - tekst reprezentaciju objekta
    definisemo metod toString() tako da vrati string sa stanjem objekta
    */
    
    public String toString(){
        return ime + " " + prezime + " " + radnoMesto + " " + plata + "" + brojRadneKnjizice;
    }
    public void ispisi(){
        System.out.println(ime + " " + prezime + " " + radnoMesto + " " + plata);
    }
}
